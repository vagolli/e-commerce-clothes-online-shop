﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using VotlisClothes.ViewModels;

namespace VotlisClothes.Models
{
    public class Order
    {
        [Required]
        [Key]
        [ScaffoldColumn(false)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [StringLength(150, MinimumLength = 1)]
        [Display(Name = "Customer : ")]
        public string CustomerId { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [Display(Name = "Order Date: ")]
        public DateTime OrderDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [Range(0,999999999)]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Display(Name = "Total Price: ")]
        public decimal TotalAmount { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(70)]  
        public string Address { get; set; }

        [Required(ErrorMessage = "City is required")]
        [StringLength(40)]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required")]
        [StringLength(40)]
        public string State { get; set; }

        [Required(ErrorMessage = "Postal Code is required")]
        [Display(Name ="Postal Code")]
        [StringLength(10)]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Country is required")]
        [StringLength(40)]
        public string Country { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        [StringLength(24)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email Address is required")]
        [Display(Name ="Email Address")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
            ErrorMessage = "Email is is not valid.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}