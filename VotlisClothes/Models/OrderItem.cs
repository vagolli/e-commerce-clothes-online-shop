﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VotlisClothes.Models
{
    public class OrderItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int Order_id { get; set; }

        [ForeignKey("Order_id")]
        public Order order { get; set; }

        [Required]
        [Display(Name = "Item No.")]
        public int ItemId { get; set; }

        [Required]
        public string Season { get; set; }

        [Required]
        [Display(Name = "Collection Group")]
        public string CollectionGroup { get; set; }

        [Required]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 9999999999999999.99)]
        public decimal ItemPrice { get; set; }

        [Required]
        [Range(0, 500)]
        public int Quantity { get; set; }

        [Range(0, 100)]
        [Display(Name = "Discount")]
        public double  Discount { get; set; }
    }
}