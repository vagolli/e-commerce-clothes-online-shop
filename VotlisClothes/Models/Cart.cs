﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VotlisClothes.Models
{
    public class Cart
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RecordId { get; set; }

        [Display(Name = "Cart Item No")]
        public string CartId { get; set; }

        [Display(Name = "Item Id:")]
        public int ItemId { get; set; }

        [Display(Name = "Total amount of items")]
        public int Count { get; set; }

        public string Season { get; set; }

        [Display(Name = "Collection Group")]
        public string CollectionGroup { get; set; }

        public System.DateTime DateCreated { get; set; }

        public virtual Item Item { get; set; }


    }
}