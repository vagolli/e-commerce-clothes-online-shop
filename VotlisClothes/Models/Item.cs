﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VotlisClothes.Models
{
    public class Item
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(250, MinimumLength = 1)]
        [Display(Name = "Item Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 2)]
        [Display(Name = "Collection Group")]
        public string CollectionGroup { get; set; }

        [StringLength(400, MinimumLength = 2)]
        [Display(Name = "Item Description")]
        public string Description { get; set; }

        [Display(Name="Image")]
        public string ImagePath { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 1)]
        public string Color { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 1)]
        public string Size { get; set; }

        [Required]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 9999999999999999.99)]
        public decimal Price { get; set; }

        [Required]
        public bool Available { get; set; }

        [Required]
        [Range(0, 99999999)]
        [Display(Name = "Quantity Available")]
        public int QuantityAvailable { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        [Display(Name ="Season: ")]
        public string SeasonId { get; set; }

        [Required]
        [Display(Name = "Discounted Item")]
        public bool HasDiscount { get; set; }

        [Display(Name = "Discount")]
        [Range(0, 100)]
        public double DiscountPercentage { get; set; }
    }
}