﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VotlisClothes.Startup))]
namespace VotlisClothes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
