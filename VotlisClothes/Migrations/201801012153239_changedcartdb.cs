namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedcartdb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Carts", "Item_Id", "dbo.Items");
            DropIndex("dbo.Carts", new[] { "Item_Id" });
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid());
            AlterColumn("dbo.OrderItems", "Order_id", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderItems", "Order_Id");
            DropTable("dbo.Carts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnonymousItemId = c.String(),
                        Count = c.Int(nullable: false),
                        Season = c.String(),
                        CollectionGroup = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Item_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            AlterColumn("dbo.OrderItems", "Order_id", c => c.Guid());
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderItems", "Order_Id");
            CreateIndex("dbo.Carts", "Item_Id");
            AddForeignKey("dbo.Carts", "Item_Id", "dbo.Items", "Id");
        }
    }
}
