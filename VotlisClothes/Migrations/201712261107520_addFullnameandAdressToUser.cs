namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFullnameandAdressToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AspNetUsers", "Address", c => c.String(nullable: false, maxLength: 250));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Address");
            DropColumn("dbo.AspNetUsers", "FullName");
        }
    }
}
