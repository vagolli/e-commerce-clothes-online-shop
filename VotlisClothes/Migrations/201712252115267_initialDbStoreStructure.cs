namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialDbStoreStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Descriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Name = c.String(),
                        Color = c.String(),
                        Size = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        HasDiscount = c.Boolean(nullable: false),
                        DiscountPercentage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Description_Id = c.Int(),
                        Discount_Id = c.Int(),
                        Stock_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Descriptions", t => t.Description_Id)
                .ForeignKey("dbo.Discounts", t => t.Discount_Id)
                .ForeignKey("dbo.Stocks", t => t.Stock_Id)
                .Index(t => t.Description_Id)
                .Index(t => t.Discount_Id)
                .Index(t => t.Stock_Id);
            
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ItemId = c.Int(nullable: false),
                        Available = c.Boolean(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StoreAgents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CanBuy = c.Boolean(nullable: false),
                        Address_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.Address_Id)
                .Index(t => t.Address_Id);
            
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ZipCode = c.String(),
                        SteetCode = c.String(),
                        TelephoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StoreAgents", "Address_Id", "dbo.Addresses");
            DropForeignKey("dbo.Items", "Stock_Id", "dbo.Stocks");
            DropForeignKey("dbo.Items", "Discount_Id", "dbo.Discounts");
            DropForeignKey("dbo.Items", "Description_Id", "dbo.Descriptions");
            DropIndex("dbo.StoreAgents", new[] { "Address_Id" });
            DropIndex("dbo.Items", new[] { "Stock_Id" });
            DropIndex("dbo.Items", new[] { "Discount_Id" });
            DropIndex("dbo.Items", new[] { "Description_Id" });
            DropTable("dbo.Addresses");
            DropTable("dbo.StoreAgents");
            DropTable("dbo.Stocks");
            DropTable("dbo.Items");
            DropTable("dbo.Discounts");
            DropTable("dbo.Descriptions");
        }
    }
}
