namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addorder : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "Description_Id", "dbo.Descriptions");
            DropForeignKey("dbo.Items", "Discount_Id", "dbo.Discounts");
            DropForeignKey("dbo.Items", "Stock_Id", "dbo.Stocks");
            DropIndex("dbo.Items", new[] { "Description_Id" });
            DropIndex("dbo.Items", new[] { "Discount_Id" });
            DropIndex("dbo.Items", new[] { "Stock_Id" });
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Guid(nullable: false),
                        ItemId = c.Int(nullable: false),
                        ItemPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Int(nullable: false),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.String(nullable: false, maxLength: 150),
                        OrderDate = c.String(nullable: false),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Items", "Name", c => c.String(nullable: false, maxLength: 250));
            AddColumn("dbo.Items", "Color", c => c.String(nullable: false, maxLength: 150));
            AddColumn("dbo.Items", "Size", c => c.String(nullable: false, maxLength: 150));
            AddColumn("dbo.Items", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Items", "Available", c => c.Boolean(nullable: false));
            AddColumn("dbo.Items", "QuantityAvailable", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "HasDiscount", c => c.Boolean(nullable: false));
            AddColumn("dbo.Items", "DiscountPercentage", c => c.Double(nullable: false));
            DropColumn("dbo.Items", "Description_Id");
            DropColumn("dbo.Items", "Discount_Id");
            DropColumn("dbo.Items", "Stock_Id");
            DropTable("dbo.Descriptions");
            DropTable("dbo.Discounts");
            DropTable("dbo.Stocks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Available = c.Boolean(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HasDiscount = c.Boolean(nullable: false),
                        DiscountPercentage = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Descriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        Color = c.String(nullable: false, maxLength: 150),
                        Size = c.String(nullable: false, maxLength: 150),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Items", "Stock_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "Discount_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Items", "Description_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Items", "DiscountPercentage");
            DropColumn("dbo.Items", "HasDiscount");
            DropColumn("dbo.Items", "QuantityAvailable");
            DropColumn("dbo.Items", "Available");
            DropColumn("dbo.Items", "Price");
            DropColumn("dbo.Items", "Size");
            DropColumn("dbo.Items", "Color");
            DropColumn("dbo.Items", "Name");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderItems");
            CreateIndex("dbo.Items", "Stock_Id");
            CreateIndex("dbo.Items", "Discount_Id");
            CreateIndex("dbo.Items", "Description_Id");
            AddForeignKey("dbo.Items", "Stock_Id", "dbo.Stocks", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Items", "Discount_Id", "dbo.Discounts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Items", "Description_Id", "dbo.Descriptions", "Id", cascadeDelete: true);
        }
    }
}
