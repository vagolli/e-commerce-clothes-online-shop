namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcart : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderItems", "Order_Id", "dbo.Orders");
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnonymousItemId = c.String(),
                        Count = c.Int(nullable: false),
                        Season = c.String(),
                        CollectionGroup = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        Item_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.Item_Id)
                .Index(t => t.Item_Id);
            
            AddColumn("dbo.OrderItems", "Season", c => c.String(nullable: false));
            AddColumn("dbo.OrderItems", "CollectionGroup", c => c.String(nullable: false));
            AlterColumn("dbo.OrderItems", "Discount", c => c.Double(nullable: false));
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid());
            AlterColumn("dbo.Orders", "OrderDate", c => c.DateTime(nullable: false));
            CreateIndex("dbo.OrderItems", "Order_Id");
            AddForeignKey("dbo.OrderItems", "Order_Id", "dbo.Orders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderItems", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Carts", "Item_Id", "dbo.Items");
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            DropIndex("dbo.Carts", new[] { "Item_Id" });
            AlterColumn("dbo.Orders", "OrderDate", c => c.String(nullable: false));
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.OrderItems", "Discount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.OrderItems", "CollectionGroup");
            DropColumn("dbo.OrderItems", "Season");
            DropTable("dbo.Carts");
            CreateIndex("dbo.OrderItems", "Order_Id");
            AddForeignKey("dbo.OrderItems", "Order_Id", "dbo.Orders", "Id", cascadeDelete: true);
        }
    }
}
