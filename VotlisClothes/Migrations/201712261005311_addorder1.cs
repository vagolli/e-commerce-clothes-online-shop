namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addorder1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderItems", "Order_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderItems", "Order_Id");
            AddForeignKey("dbo.OrderItems", "Order_Id", "dbo.Orders", "Id", cascadeDelete: true);
            DropColumn("dbo.OrderItems", "OrderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderItems", "OrderId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.OrderItems", "Order_Id", "dbo.Orders");
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            DropColumn("dbo.OrderItems", "Order_Id");
        }
    }
}
