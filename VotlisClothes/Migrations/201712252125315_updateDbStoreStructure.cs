namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDbStoreStructure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StoreAgents", "Address_Id", "dbo.Addresses");
            DropIndex("dbo.StoreAgents", new[] { "Address_Id" });
            DropTable("dbo.StoreAgents");
            DropTable("dbo.Addresses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ZipCode = c.String(),
                        SteetCode = c.String(),
                        TelephoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StoreAgents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CanBuy = c.Boolean(nullable: false),
                        Address_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.StoreAgents", "Address_Id");
            AddForeignKey("dbo.StoreAgents", "Address_Id", "dbo.Addresses", "Id");
        }
    }
}
