namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemupdateprops : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "CollectionGroup", c => c.String(nullable: false, maxLength: 150));
            AddColumn("dbo.Items", "Description", c => c.String(maxLength: 400));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "Description");
            DropColumn("dbo.Items", "CollectionGroup");
        }
    }
}
