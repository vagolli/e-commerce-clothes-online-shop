namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addimagetoCollection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CollectionGroups", "ImagePath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CollectionGroups", "ImagePath");
        }
    }
}
