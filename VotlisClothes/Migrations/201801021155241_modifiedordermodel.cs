namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifiedordermodel : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            AddColumn("dbo.Orders", "Address", c => c.String(nullable: false, maxLength: 70));
            AddColumn("dbo.Orders", "City", c => c.String(nullable: false, maxLength: 40));
            AddColumn("dbo.Orders", "State", c => c.String(nullable: false, maxLength: 40));
            AddColumn("dbo.Orders", "PostalCode", c => c.String(nullable: false, maxLength: 10));
            AddColumn("dbo.Orders", "Country", c => c.String(nullable: false, maxLength: 40));
            AddColumn("dbo.Orders", "Phone", c => c.String(nullable: false, maxLength: 24));
            AddColumn("dbo.Orders", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid());
            AlterColumn("dbo.OrderItems", "Order_id", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderItems", "Order_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            AlterColumn("dbo.OrderItems", "Order_id", c => c.Guid());
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid(nullable: false));
            DropColumn("dbo.Orders", "Email");
            DropColumn("dbo.Orders", "Phone");
            DropColumn("dbo.Orders", "Country");
            DropColumn("dbo.Orders", "PostalCode");
            DropColumn("dbo.Orders", "State");
            DropColumn("dbo.Orders", "City");
            DropColumn("dbo.Orders", "Address");
            CreateIndex("dbo.OrderItems", "Order_Id");
        }
    }
}
