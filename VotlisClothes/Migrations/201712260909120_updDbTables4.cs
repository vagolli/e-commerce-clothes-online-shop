namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updDbTables4 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Items");
            AddColumn("dbo.Items", "SeasonId", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Items", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Items", "Id");
            DropColumn("dbo.Descriptions", "ItemId");
            DropColumn("dbo.Discounts", "ItemId");
            DropColumn("dbo.Stocks", "ItemId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Stocks", "ItemId", c => c.Int(nullable: false));
            AddColumn("dbo.Discounts", "ItemId", c => c.Int(nullable: false));
            AddColumn("dbo.Descriptions", "ItemId", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.Items");
            AlterColumn("dbo.Items", "Id", c => c.Byte(nullable: false));
            DropColumn("dbo.Items", "SeasonId");
            AddPrimaryKey("dbo.Items", "Id");
        }
    }
}
