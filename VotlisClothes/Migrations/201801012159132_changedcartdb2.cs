namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedcartdb2 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        RecordId = c.Int(nullable: false, identity: true),
                        CartId = c.String(),
                        ItemId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        Season = c.String(),
                        CollectionGroup = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.RecordId)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId);
            
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid());
            AlterColumn("dbo.OrderItems", "Order_id", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderItems", "Order_Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Carts", "ItemId", "dbo.Items");
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            DropIndex("dbo.Carts", new[] { "ItemId" });
            AlterColumn("dbo.OrderItems", "Order_id", c => c.Guid());
            AlterColumn("dbo.OrderItems", "Order_Id", c => c.Guid(nullable: false));
            DropTable("dbo.Carts");
            CreateIndex("dbo.OrderItems", "Order_Id");
        }
    }
}
