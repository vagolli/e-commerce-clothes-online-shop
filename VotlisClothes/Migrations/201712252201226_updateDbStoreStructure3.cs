namespace VotlisClothes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDbStoreStructure3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "Description_Id", "dbo.Descriptions");
            DropForeignKey("dbo.Items", "Discount_Id", "dbo.Discounts");
            DropForeignKey("dbo.Items", "Stock_Id", "dbo.Stocks");
            DropIndex("dbo.Items", new[] { "Description_Id" });
            DropIndex("dbo.Items", new[] { "Discount_Id" });
            DropIndex("dbo.Items", new[] { "Stock_Id" });
            AlterColumn("dbo.Descriptions", "Name", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Descriptions", "Color", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Descriptions", "Size", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Discounts", "DiscountPercentage", c => c.Double(nullable: false));
            AlterColumn("dbo.Items", "Description_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Items", "Discount_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Items", "Stock_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Items", "Description_Id");
            CreateIndex("dbo.Items", "Discount_Id");
            CreateIndex("dbo.Items", "Stock_Id");
            AddForeignKey("dbo.Items", "Description_Id", "dbo.Descriptions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Items", "Discount_Id", "dbo.Discounts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Items", "Stock_Id", "dbo.Stocks", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "Stock_Id", "dbo.Stocks");
            DropForeignKey("dbo.Items", "Discount_Id", "dbo.Discounts");
            DropForeignKey("dbo.Items", "Description_Id", "dbo.Descriptions");
            DropIndex("dbo.Items", new[] { "Stock_Id" });
            DropIndex("dbo.Items", new[] { "Discount_Id" });
            DropIndex("dbo.Items", new[] { "Description_Id" });
            AlterColumn("dbo.Items", "Stock_Id", c => c.Int());
            AlterColumn("dbo.Items", "Discount_Id", c => c.Int());
            AlterColumn("dbo.Items", "Description_Id", c => c.Int());
            AlterColumn("dbo.Discounts", "DiscountPercentage", c => c.Int(nullable: false));
            AlterColumn("dbo.Descriptions", "Size", c => c.String());
            AlterColumn("dbo.Descriptions", "Color", c => c.String());
            AlterColumn("dbo.Descriptions", "Name", c => c.String());
            CreateIndex("dbo.Items", "Stock_Id");
            CreateIndex("dbo.Items", "Discount_Id");
            CreateIndex("dbo.Items", "Description_Id");
            AddForeignKey("dbo.Items", "Stock_Id", "dbo.Stocks", "Id");
            AddForeignKey("dbo.Items", "Discount_Id", "dbo.Discounts", "Id");
            AddForeignKey("dbo.Items", "Description_Id", "dbo.Descriptions", "Id");
        }
    }
}
