﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VotlisClothes.Models;

namespace VotlisClothes.ViewModels
{
    public class OrderViewModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Customer : ")]
        public string CustomerId { get; set; }

        [Display(Name = "Order Date: ")]
        public string OrderDate { get; set; }

        [Display(Name = "Items Purchased")]
        public List<Item> ItemsPurchased { get; set; }

        [Display(Name = "Total Price: ")]
        public decimal TotalAmount { get; set; }
    }
}