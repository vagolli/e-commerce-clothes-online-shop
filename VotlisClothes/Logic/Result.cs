﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VotlisClothes.Logic
{
    public class Result
    {
        public bool IsValid { get; set; }
        public string Message { get; set; }
    }
}