﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using VotlisClothes.Models;
using System.Threading.Tasks;
using VotlisClothes.ViewModels;
using System.Data.Entity;

namespace VotlisClothes.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {
        private ApplicationRoleManger _roleManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext _context;


        public RoleController()
        {
            _context = new ApplicationDbContext();
        }

        public RoleController(ApplicationRoleManger roleManger,ApplicationUserManager userManager)
        {
            RoleManger = roleManger;
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManger RoleManger
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManger>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        // GET: Role
        public ActionResult Index()
        {
            List<RoleViewModel> list = new List<RoleViewModel>();
            foreach (var role in RoleManger.Roles)
                list.Add(new RoleViewModel(role));

            return View(list);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel model)
        {
            var role = new ApplicationRole() { Name = model.Name };
            await RoleManger.CreateAsync(role);
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(string id)
        {
            var role = await RoleManger.FindByIdAsync(id);
            return View(new RoleViewModel(role));
        }

        [HttpPost]
        public async Task<ActionResult> Edit(RoleViewModel model)
        {
            var role = new ApplicationRole() { Id = model.Id, Name = model.Name };
            await RoleManger.UpdateAsync(role);
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Details(string id)
        {
            var role = await RoleManger.FindByIdAsync(id);
            return View(new RoleViewModel(role));
        }

        public async Task<ActionResult> Delete(string id)
        {
            var role = await RoleManger.FindByIdAsync(id);
            return View(new RoleViewModel(role));
        }

        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(string id)
        {
            var role = await RoleManger.FindByIdAsync(id);
            await RoleManger.DeleteAsync(role);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult RegisterRole()
        {
            ViewBag.Name = new SelectList(_context.Roles.ToList(), "Name", "Name");
            ViewBag.FullName = new SelectList(_context.Users.ToList(), "FullName", "FullName");
            return View();
        }

        //POST: /Role/RegisterRole
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterRole(RoleViewModel model, ApplicationUser user)
        {
            var userId = _context.Users.Where(i => i.FullName == user.FullName).Select(s => s.Id);
            string IdwithRole = string.Empty;
            foreach (var i in userId)
                IdwithRole = i.ToString();

            await this.UserManager.AddToRoleAsync(IdwithRole, model.Name);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult UserRoles()
        {
            var usersWithRoles = (from user in _context.Users
                                  select new
                                  {
                                      Id = user.Id,
                                      FullName = user.FullName,
                                      UserName = user.UserName,
                                      RoleNames = (from userRole in user.Roles
                                                   join role in _context.Roles on userRole.RoleId equals role.Id
                                                   select role.Name).ToList()
                                  }).ToList().Select(p => new UserRoleViewModel()
                                  {
                                      Id = p.Id,
                                      FullName = p.FullName,
                                      UserName = p.UserName,
                                      Role = string.Join(",", p.RoleNames)
                                  });

            return View(usersWithRoles);
        }

        [HttpPost]
        public async Task<ActionResult> UserRoles(string Id)
        {
            var userRoles = await UserManager.GetRolesAsync(Id);
            foreach (var role in userRoles)
            {
                await UserManager.RemoveFromRoleAsync(Id, role);
            }
            return RedirectToAction("UserRoles", "Role");
        }
    }
}