﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using VotlisClothes.Models;

namespace VotlisClothes.Controllers
{
    [Authorize(Roles = "Employee")]
    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Item
        public async Task<ActionResult> Index(string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.CollectionGroupSortParm = sortOrder == "CollectionGruop" ? "CollectionGruop_desc" : "CollectionGruop";
            ViewBag.SeasonSortParm = sortOrder == "Season" ? "Season_desc" : "Season";
            ViewBag.PriceSortParm = sortOrder == "Price" ? "Price_desc" : "Price";
            ViewBag.QtySortParm = sortOrder == "Quantity" ? "Quantity_desc" : "Quantity";

            var getItems = new APIControllers.ItemsController().GetItems(sortOrder, searchString);
            return View(await getItems.ToListAsync());
        }

        // GET: Item/Details/5
        public ActionResult Details(int id)
        {
            var getItem = new APIControllers.ItemsController().GetItem(id);
            var result = getItem as OkNegotiatedContentResult<Item>;
            return View(result.Content);
        }

        // GET: Item/Create
        public ActionResult Create()
        {
            ViewBag.Seasons = new SelectList(db.Seasons, "Name", "Name");
            ViewBag.CollectionGroups = new SelectList(db.CollectionGroups, "Name", "Name");
            return View();
        }

        // POST: Item/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,CollectionGroup,Description,ImagePath,Color,Size,Price,Available,QuantityAvailable,SeasonId,HasDiscount,DiscountPercentage")] Item item)
        {
            var createItem = new APIControllers.ItemsController().PostItem(item);
            //var result = createItem as CreatedAtRouteNegotiatedContentResult<Item>;
            return RedirectToAction("Index");
        }

        // GET: Item/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.Seasons = new SelectList(db.Seasons, "Name", "Name");
            ViewBag.CollectionGroups = new SelectList(db.CollectionGroups, "Name", "Name");
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Item item = await db.Items.FindAsync(id);
            if (item == null)
                return HttpNotFound();
            return View(item);
        }

        // POST: Item/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CollectionGroup,Description,ImagePath,Color,Size,Price,Available,QuantityAvailable,SeasonId,HasDiscount,DiscountPercentage")] Item item)
        {
            ViewBag.Seasons = new SelectList(db.Seasons, "Name", "Name");
            ViewBag.CollectionGroups = new SelectList(db.CollectionGroups, "Name", "Name");
            var editItem = new APIControllers.ItemsController().PutItem(item.Id,item);
            var result = editItem as OkNegotiatedContentResult<Item>;
            return View(result.Content);
        }

        // GET: Item/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Item item = await db.Items.FindAsync(id);
            if (item == null)
                return HttpNotFound();
            return View(item);
        }

        // POST: Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var deleteItem = new APIControllers.ItemsController().DeleteItem(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
