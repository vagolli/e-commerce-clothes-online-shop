﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using VotlisClothes.Models;

namespace VotlisClothes.Controllers
{
    public class CatalogueController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Catalogue
        public async Task<ActionResult> Index()
        {
            var CollectionGroups = await db.CollectionGroups.ToListAsync();
            return View(CollectionGroups);
        }
        //
        // GET: /Catalogue/CollectionMenu
        [ChildActionOnly]
        public ActionResult CollectionMenu()
        {
            var collections = db.CollectionGroups.ToList();
            return PartialView(collections);
        }
        //
        // GET: /Catalogue/Browse?CollectionGroup=Summer2017
        public async Task<ActionResult> Browse(string CollectionName, string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.CollectionGroupSortParm = sortOrder == "CollectionGruop" ? "CollectionGruop_desc" : "CollectionGruop";
            ViewBag.SeasonSortParm = sortOrder == "Season" ? "Season_desc" : "Season";
            ViewBag.PriceSortParm = sortOrder == "Price" ? "Price_desc" : "Price";
            ViewBag.QtySortParm = sortOrder == "Quantity" ? "Quantity_desc" : "Quantity";

            var getItems = new APIControllers.CatalogueController().GetItems(CollectionName,sortOrder, searchString);
            return View(await getItems.ToListAsync());
        }
        //
        // GET: /Catalogue/Details/1
        public  ActionResult Details(int id)
        {
            var getItem = new APIControllers.ItemsController().GetItem(id);
            var result = getItem as OkNegotiatedContentResult<Item>;
            return View( result.Content);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}