﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using VotlisClothes.Models;

namespace VotlisClothes.Controllers.APIControllers
{
    public class ItemsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Item
        public IQueryable<Item> GetItems(string sortOrder, string searchString)
        {
            IQueryable<Item> Items = db.Items;

            if (!String.IsNullOrEmpty(searchString))
            {
                Items = Items.Where(i => i.Name.Contains(searchString)
                                       || i.CollectionGroup.Contains(searchString)
                                       || i.SeasonId.Contains(searchString)
                                       || i.Description.Contains(searchString)
                                       || i.Color.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "Name_desc":
                    Items = Items.OrderByDescending(i => i.Name);
                    break;
                case "CollectionGruop_desc":
                    Items = Items.OrderByDescending(i => i.CollectionGroup);
                    break;
                case "CollectionGruop":
                    Items = Items.OrderBy(i => i.CollectionGroup);
                    break;
                case "Season_desc":
                    Items = Items.OrderByDescending(i => i.SeasonId);
                    break;
                case "Season":
                    Items = Items.OrderBy(i => i.SeasonId);
                    break;
                case "Price_desc":
                    Items = Items.OrderByDescending(i => i.Price);
                    break;
                case "Price":
                    Items = Items.OrderBy(i => i.Price);
                    break;
                case "Quantity_desc":
                    Items = Items.OrderByDescending(i => i.QuantityAvailable);
                    break;
                case "Quantity":
                    Items = Items.OrderBy(i => i.QuantityAvailable);
                    break;
                default:
                    Items = Items.OrderBy(i => i.Name);
                    break;
            }

            return Items;
        }

        // GET: api/Item/5
        [ResponseType(typeof(Item))]
        public IHttpActionResult GetItem(int id)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }
        

        // PUT: api/Item/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutItem(int id, Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.Id)
            {
                return BadRequest();
            }
            item.ImagePath = $"\\Pictures\\{item.ImagePath}";
            db.Entry(item).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(item);
        }

        // POST: api/Item
        [ResponseType(typeof(Item))]
        public IHttpActionResult PostItem(Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            item.ImagePath = $"\\Pictures\\{item.ImagePath}";
            db.Items.Add(item);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = item.Id }, item);
        }

        // DELETE: api/Item/5
        [ResponseType(typeof(Item))]
        public IHttpActionResult DeleteItem(int id)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            db.Items.Remove(item);
            db.SaveChanges();

            return Ok(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(int id)
        {
            return db.Items.Count(e => e.Id == id) > 0;
        }
    }
}