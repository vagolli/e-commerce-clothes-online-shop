﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VotlisClothes.Models;

namespace VotlisClothes.Controllers.APIControllers
{
    public class CatalogueController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Item
        public IQueryable<Item> GetItems(string CollectionName, string sortOrder, string searchString)
        {
            IQueryable<Item> ColectItems = (from Items in db.Items.Where(x => x.CollectionGroup == CollectionName)
                                                          select Items);

            if (!String.IsNullOrEmpty(searchString))
            {
                ColectItems = ColectItems.Where(i => i.Name.Contains(searchString)
                                       || i.CollectionGroup.Contains(searchString)
                                       || i.SeasonId.Contains(searchString)
                                       || i.Description.Contains(searchString)
                                       || i.Color.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "Name_desc":
                    ColectItems = ColectItems.OrderByDescending(i => i.Name);
                    break;
                case "CollectionGruop_desc":
                    ColectItems = ColectItems.OrderByDescending(i => i.CollectionGroup);
                    break;
                case "CollectionGruop":
                    ColectItems = ColectItems.OrderBy(i => i.CollectionGroup);
                    break;
                case "Season_desc":
                    ColectItems = ColectItems.OrderByDescending(i => i.SeasonId);
                    break;
                case "Season":
                    ColectItems = ColectItems.OrderBy(i => i.SeasonId);
                    break;
                case "Price_desc":
                    ColectItems = ColectItems.OrderByDescending(i => i.Price);
                    break;
                case "Price":
                    ColectItems = ColectItems.OrderBy(i => i.Price);
                    break;
                default:
                    ColectItems = ColectItems.OrderBy(i => i.Name);
                    break;
            }

            return ColectItems;
        }

        // GET: api/Item/5
        [ResponseType(typeof(Item))]
        public IHttpActionResult GetItem(int id)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        //// PUT: api/Item/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutItem(int id, Item item)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != item.Id)
        //    {
        //        return BadRequest();
        //    }
        //    item.ImagePath = $"\\Pictures\\{item.ImagePath}";
        //    db.Entry(item).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ItemExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Ok(item);
        //}

        //// POST: api/Item
        //[ResponseType(typeof(Item))]
        //public IHttpActionResult PostItem(Item item)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    item.ImagePath = $"\\Pictures\\{item.ImagePath}";
        //    db.Items.Add(item);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = item.Id }, item);
        //}

        //// DELETE: api/Item/5
        //[ResponseType(typeof(Item))]
        //public IHttpActionResult DeleteItem(int id)
        //{
        //    Item item = db.Items.Find(id);
        //    if (item == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Items.Remove(item);
        //    db.SaveChanges();

        //    return Ok(item);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(int id)
        {
            return db.Items.Count(e => e.Id == id) > 0;
        }
    }
}
