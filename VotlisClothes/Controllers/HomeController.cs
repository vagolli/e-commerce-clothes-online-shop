﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VotlisClothes.Models;

namespace VotlisClothes.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public HomeController()
        {
        }
        public ActionResult Index()
        {
            ViewBag.CollectionGroups = new SelectList(db.CollectionGroups, "Name", "Name");

            IEnumerable<Item> items = db.Items.OrderBy(i=>i.Name).ToList();
            return View(items);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}